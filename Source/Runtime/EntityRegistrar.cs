using System;
using System.Collections.Generic;

namespace Manganese.EntityKit {
  public static class EntityRegistrar {
    private static Dictionary<string, Type> entityTypes = new Dictionary<string, Type>();

    public static void RegisterEntityType(string entityTypeKey, Type entityType) {
      entityTypes[entityTypeKey] = entityType;
    }

    public static Type GetEntityType(string entityTypeKey) {
      return entityTypes[entityTypeKey];
    }
  }
}