namespace Manganese.EntityKit {
	public interface IEntitiesPageSearchParameters {
		uint pageSize { get; set; }
		uint page { get; set; }
	}
}