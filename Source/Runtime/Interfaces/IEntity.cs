namespace Manganese.EntityKit {
	public interface IEntity {
		uint id { get; set; }
	}
}