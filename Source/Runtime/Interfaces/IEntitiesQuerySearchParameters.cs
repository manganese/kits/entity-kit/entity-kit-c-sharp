namespace Manganese.EntityKit {
	public interface IEntitiesQuerySearchParameters {
		string query { get; set; }
	}
}