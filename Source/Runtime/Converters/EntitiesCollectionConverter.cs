using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using UnityEngine;

namespace Manganese.EntityKit {
  public class EntitiesCollectionConverter : JsonConverter<EntitiesCollection> {
    private static readonly string ENTITIES_KEY = "entities";

    public override void WriteJson(JsonWriter writer, EntitiesCollection entitiesCollection, JsonSerializer serializer) {
      var entitiesCollectionJObject = new JObject();
      var entitiesJObject = new JObject();

      foreach (var entityType in entitiesCollection.GetEntityTypes()) {
        var entitiesOfType = entitiesCollection.GetEntitiesOfType<IEntity>(entityType);
        var entitiesOfTypeJObject = new JObject();

        foreach (var pair in entitiesOfType) {
          entitiesOfTypeJObject.Add(pair.Key.ToString(), JObject.FromObject(pair.Value));
        }

        entitiesJObject.Add(entityType, entitiesOfTypeJObject);
      }

      entitiesCollectionJObject.Add(ENTITIES_KEY, entitiesJObject);
      entitiesCollectionJObject.WriteTo(writer);
    }

    public override EntitiesCollection ReadJson(JsonReader reader, Type objectType, EntitiesCollection _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        return null;
      } else if (reader.TokenType == JsonToken.StartObject) {
        var entitiesCollectionJObject = JObject.Load(reader);

        if (!entitiesCollectionJObject.ContainsKey(ENTITIES_KEY)) {
          throw new JsonException();
        }

        var entitiesCollection = new EntitiesCollection();
        var entitiesJObject = entitiesCollectionJObject[ENTITIES_KEY].Value<JObject>();

        foreach (var entityTypeJProperty in entitiesJObject.Properties()) {
          var entityTypeKey = entityTypeJProperty.Name.ToString();
          var entityType = EntityRegistrar.GetEntityType(entityTypeKey);
          var entitiesOfTypeJObject = entitiesJObject[entityTypeKey].Value<JObject>();

          foreach (var idJProperty in entitiesOfTypeJObject.Properties()) {
            var id = UInt32.Parse(idJProperty.Name.ToString());
            var entityJObject = entitiesOfTypeJObject[id.ToString()].Value<JObject>();
            var entity = (IEntity) JsonConvert.DeserializeObject(entityJObject.ToString(), entityType);

            entitiesCollection.UnsafePutEntity(entityTypeKey, entity);
          }
        }

        return entitiesCollection;
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}