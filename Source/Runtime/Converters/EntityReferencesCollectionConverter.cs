using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using UnityEngine;

namespace Manganese.EntityKit {
  public class EntityReferencesCollectionConverter : JsonConverter<EntityReferencesCollection> {
    private static readonly string ENTITY_IDS_KEY = "entityIds";

    public override void WriteJson(JsonWriter writer, EntityReferencesCollection entityReferencesCollection, JsonSerializer serializer) {
      var entityReferencesCollectionJObject = new JObject();
      var entityIdsJObject = new JObject();

      foreach (var entityType in entityReferencesCollection.GetEntityTypes()) {
        var entityIdsOfType = entityReferencesCollection.GetEntityIdsOfType(entityType);
        var entityIdsOfTypeJArray = new JArray();

        foreach (var id in entityIdsOfType) {
          entityIdsOfTypeJArray.Add(id);
        }

        entityIdsJObject.Add(entityType, entityIdsOfTypeJArray);
      }

      entityReferencesCollectionJObject.Add(ENTITY_IDS_KEY, entityIdsJObject);
      entityReferencesCollectionJObject.WriteTo(writer);
    }

    public override EntityReferencesCollection ReadJson(JsonReader reader, Type objectType, EntityReferencesCollection _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        return null;
      } else if (reader.TokenType == JsonToken.StartObject) {
        var entityReferencesCollectionJObject = JObject.Load(reader);

        if (!entityReferencesCollectionJObject.ContainsKey(ENTITY_IDS_KEY)) {
          throw new JsonException();
        }

        var entityReferencesCollection = new EntityReferencesCollection();
        var entityIdsJObject = entityReferencesCollectionJObject[ENTITY_IDS_KEY].Value<JObject>();

        foreach (var entityTypeJProperty in entityIdsJObject.Properties()) {
          var entityType = entityTypeJProperty.Name.ToString();
          var entityIdsOfTypeJArray = entityIdsJObject[entityType].Value<JArray>();

          foreach (var id in entityIdsOfTypeJArray.Values<uint>()) {
            entityReferencesCollection.UnsafePutEntityId(entityType, id);
          }
        }

        return entityReferencesCollection;
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}