using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using UnityEngine;

namespace Manganese.EntityKit {
  public class EntitiesUpdateConverter : JsonConverter<EntitiesUpdate> {
    private static readonly string UPDATED_ENTITIES_KEY = "updatedEntities";
    private static readonly string REMOVED_ENTITIES_KEY = "removedEntities";

    public override void WriteJson(JsonWriter writer, EntitiesUpdate entitiesUpdate, JsonSerializer serializer) {
      var entitiesUpdateJObject = new JObject();
      entitiesUpdateJObject.Add(UPDATED_ENTITIES_KEY, JObject.FromObject(entitiesUpdate.UnsafeGetUpdatedEntities()));
      entitiesUpdateJObject.Add(REMOVED_ENTITIES_KEY, JObject.FromObject(entitiesUpdate.UnsafeGetRemovedEntities()));
      entitiesUpdateJObject.WriteTo(writer);
    }

    public override EntitiesUpdate ReadJson(JsonReader reader, Type objectType, EntitiesUpdate _existingValue, bool _hasExistingValue, JsonSerializer serializer) {
      if (reader.TokenType == JsonToken.Null) {
        return null;
      } else if (reader.TokenType == JsonToken.StartObject) {
        var entitiesUpdateJObject = JObject.Load(reader);

        if (
          !entitiesUpdateJObject.ContainsKey(UPDATED_ENTITIES_KEY) ||
          !entitiesUpdateJObject.ContainsKey(REMOVED_ENTITIES_KEY)
        ) {
          throw new JsonException();
        }

        var updatedEntitiesJObject = entitiesUpdateJObject[UPDATED_ENTITIES_KEY].Value<JObject>();
        var updatedEntities = (EntitiesCollection) JsonConvert.DeserializeObject(updatedEntitiesJObject.ToString(), typeof(EntitiesCollection));

        var removedEntitiesJObject = entitiesUpdateJObject[REMOVED_ENTITIES_KEY].Value<JObject>();
        var removedEntities = (EntityReferencesCollection) JsonConvert.DeserializeObject(removedEntitiesJObject.ToString(), typeof(EntityReferencesCollection));

        var entitiesUpdate = new EntitiesUpdate();
        entitiesUpdate.UnsafeSetUpdatedEntities(updatedEntities);
        entitiesUpdate.UnsafeSetRemovedEntities(removedEntities);

        return entitiesUpdate;
      }

      throw new JsonException();
    }

    public override bool CanWrite {
      get {
        return true;
      }
    }

    public override bool CanRead {
      get {
        return true;
      }
    }
  }
}