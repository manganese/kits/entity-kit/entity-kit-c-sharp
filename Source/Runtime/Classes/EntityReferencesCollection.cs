using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using UnityEngine;


namespace Manganese.EntityKit {
  /**
   * @brief A collection of references to entities, including each entity's type and ID.
   */
  [CreateAssetMenu(
    fileName = "Entity References Collection",
    menuName = "Manganese/EntityKit/Entity References Collection",
    order = 2
  )]
  [JsonConverter(typeof(EntityReferencesCollectionConverter))]
  public class EntityReferencesCollection : ScriptableObject, IEnumerable<KeyValuePair<string, uint>>, IEquatable<EntityReferencesCollection> {
    /// @name Constructors
    /// @{
    /**
     * @brief Create a new, empty entity references collection.
     *
     * Example:
     *
     * ```csharp
     * var entityReferencesCollection = new EntityReferencesCollection();
     * ```
     */
    public EntityReferencesCollection() {}

    /**
     * @brief Create a new entity references collection and copy the data from an existing instance.
     *
     * @param entitiesCollection  The entity references collection from which to copy data.
     *
     * Example:
     *
     * ```csharp
     * var entityReferencesCollection1 = new EntityReferencesCollection().PutEntityId("widgets", 1);
     * var entityReferencesCollection2 = new EntityReferencesCollection(entityReferencesCollection1);
     * entities2.HasEntityId("widgets", 1); // true
     * ```
     */
    public EntityReferencesCollection(EntityReferencesCollection entityReferencesCollection) {
      foreach (var entityType in entityReferencesCollection.GetEntityTypes()) {
        var entityIdsOfType = entityReferencesCollection.GetEntityIdsOfType(entityType);

        this.entityIds[entityType] = new HashSet<uint>(entityIdsOfType);
      }
    }
    /// @}

    // Private state
    private Dictionary<string /* entity type */, HashSet<uint /* entity ID */>> entityIds = new Dictionary<string, HashSet<uint>>();

    /// @name Unsafe
    /// @{
    /**
     * @brief Get the internal entity IDs data structure used by the entity references collection.
     *
     * @warning Note that the reference is preserved so modifying the value returned by this method can also affect the entity references collection.
     */
    public Dictionary<string, HashSet<uint>> UnsafeGetEntityIds() {
      return this.entityIds;
    }

    /**
     * @brief Overwrite the internal entity IDs data structure used by the entity references collection.
     *
     * @warning Note that calling this method may change the values of references to this entity references collection.
     *
     * @param entityIds  The new entity IDs data to set.
     */
    public void UnsafeSetEntityIds(Dictionary<string, HashSet<uint>> entityIds) {
      this.entityIds = entityIds;
    }

    /**
     * @brief Upsert an entity ID of a given entity type in the entity references collection.  The entity references collection is modified.
     *
     * Example:
     *
     * ```csharp
     * var entityReferencesCollection = new EntityReferencesCollection();
     * entityReferencesCollection.UnsafePutEntityId("widgets", 1);
     * entityReferencesCollection.HasEntityId("widgets", 1); // true
     * ```
     *
     * @param entityType  The entity type of the entity ID to put.
     * @param id  The entity ID to put.
     */
    public void UnsafePutEntityId(string entityType, uint id) {
      HashSet<uint> entityIdsOfType;

      if (this.entityIds.ContainsKey(entityType)) {
        entityIdsOfType = this.entityIds[entityType];
      } else {
        entityIdsOfType = new HashSet<uint>();

        this.entityIds[entityType] = entityIdsOfType;
      }

      entityIdsOfType.Add(id);
    }

    /**
     * @brief Remove an entity ID of a given type by its ID from this entity references collection.  The entity references collection is modified.
     *
     * Example:
     *
     * ```csharp
     * var entityReferencesCollection = new EntityReferencesCollection().PutEntityId("widgets", 1);
     * entityReferencesCollection.HasEntityId("widgets", 1); // widget1
     * entityReferencesCollection.UnsafeRemoveEntityId("widgets", 1);
     * entityReferencesCollection.HasEntityId("widgets", 1); // null
     * ```
     *
     * @param entityType  The entity type of the entity to remove.
     * @param id  The entity ID to remove.
     */
    public void UnsafeRemoveEntityId(string entityType, uint id) {
      var entityIdsOfType = this.entityIds[entityType];

      if (entityIdsOfType == null) {
        return;
      }

      entityIdsOfType.Remove(id);

      if (entityIdsOfType.Count == 0) {
        this.entityIds.Remove(entityType);
      }
    }
    /// @}

    /// @name Access
    /// @{
    /**
     * @brief Get the types of entities contained in this entity references collection.
     *
     * Example:
     *
     * ```csharp
     * var entityReferencesCollection =
     *   new EntityReferencesCollection()
     *   .PutEntityId("widgets", 1)
     *   .PutEntityId("thingies", 1);
     * entityReferencesCollection.GetEntityTypes(); // HashSet<string> { "widgets", "thingies" }
     * ```
     */
    public HashSet<string> GetEntityTypes() {
      return new HashSet<string>(this.entityIds.Keys);
    }

    /**
     * @brief Check if an entity ID of a given entity type is contained in this entity references collection.
     *
     * @param entityType  The entity type of the entity to check for.
     * @param id  The entity ID to check for.
     */
    public bool HasEntityId(string entityType, uint id) {
      if (!this.entityIds.ContainsKey(entityType)) {
        return false;
      }

      var entityIdsOfType = this.entityIds[entityType];

      return entityIdsOfType.Contains(id);
    }

    /**
     * @brief Get all entity IDs of a given entity type contained in this entity references collection.
     *
     * @param entityType  The entity type of the entity IDs to get.
     */
    public HashSet<uint> GetEntityIdsOfType(string entityType) {
      if (!this.entityIds.ContainsKey(entityType)) {
        return new HashSet<uint>();
      }

      return new HashSet<uint>(this.entityIds[entityType]);
    }
    /// @}

    /// @name Functional Mutation
    /// @{
    /**
     * @brief Put a single entity ID of a given entity type into a copy of this entity references collection.
     *
     * @param entityType  The entity type of the entity ID to put.
     * @param id  The entity ID to put.
     */
    public EntityReferencesCollection PutEntityId(string entityType, uint id) {
      var entityReferencesCollection = new EntityReferencesCollection(this);
      entityReferencesCollection.UnsafePutEntityId(entityType, id);

      return entityReferencesCollection;
    }

    /**
     * @brief Put multiple entity IDs of a given entity type into a copy of this entity references collection.
     *
     * @param entityType  The entity type of the entity IDs to put.
     * @param ids  The entity IDs of the given type to put.
     */
    public EntityReferencesCollection PutEntityIdsOfType(string entityType, HashSet<uint> ids) {
      var entityReferencesCollection = new EntityReferencesCollection(this);

      foreach (var id in ids) {
        entityReferencesCollection.UnsafePutEntityId(entityType, id);
      }

      return entityReferencesCollection;
    }

    /**
     * @brief Put entity IDs of multiple entity types into a copy of this entity references collection.
     *
     * @param ids  A dictionary of entity types to sets of entity IDs to put.
     */
    public EntityReferencesCollection PutEntityIds(Dictionary<string, HashSet<uint>> ids) {
      var entityReferencesCollection = new EntityReferencesCollection(this);

      foreach (var entityType in ids.Keys) {
        var idsOfType = ids[entityType];

        foreach (var id in idsOfType) {
          entityReferencesCollection.UnsafePutEntityId(entityType, id);
        }
      }

      return entityReferencesCollection;
    }

    /**
     * @brief Remove a single entity ID of a given entity type from a copy of this entity references collection.
     *
     * @param entityType  The entity type of the entity ID to remove.
     * @param id  The entity ID of the given type to remove.
     */
    public EntityReferencesCollection RemoveEntityId(string entityType, uint id) {
      var entityReferencesCollection = new EntityReferencesCollection(this);
      entityReferencesCollection.UnsafeRemoveEntityId(entityType, id);

      return entityReferencesCollection;
    }

    /**
     * @brief Remove multiple entity IDs of a given entity type from a copy of this entity references collection.
     *
     * @param entityType  The entity type of the entity IDs to remove.
     * @param ids  The entity IDs of the given type to remove.
     */
    public EntityReferencesCollection RemoveEntityIdsOfType(string entityType, HashSet<uint> ids) {
      var entityReferencesCollection = new EntityReferencesCollection(this);

      foreach (var id in ids) {
        entityReferencesCollection.UnsafeRemoveEntityId(entityType, id);
      }

      return entityReferencesCollection;
    }

    /**
     * @brief Remove entity IDs which are referenced by another given entity references collection from a copy of this entity references collection.
     *
     * @param entityReferencesCollection  The entity references collection of entity IDs to remove.
     */
    public EntityReferencesCollection RemoveEntityIds(EntityReferencesCollection entityReferencesCollection) {
      var newEntityReferencesCollection = new EntityReferencesCollection(this);

      foreach (var pair in entityReferencesCollection) {
        newEntityReferencesCollection.UnsafeRemoveEntityId(pair.Key, pair.Value);
      }

      return newEntityReferencesCollection;
    }
    /// @}

    /// @name Miscellaneous
    /// @{
    /***
     * @brief Merge another entity references collection into a copy of this entity references collection.
     *
     * @param entityReferencesCollection  The entity references collection to merge with.
     */
    public EntityReferencesCollection Merge(EntityReferencesCollection entityReferencesCollection) {
      var mergedEntityReferencesCollection = new EntityReferencesCollection(this);

      foreach (var pair in entityReferencesCollection) {
        mergedEntityReferencesCollection.UnsafePutEntityId(pair.Key, pair.Value);
      }

      return mergedEntityReferencesCollection;
    }

    /***
     * @brief Determines whether at least one entity ID of any type is contained in this entity references collection.
     */
    public bool Empty {
      get {
        foreach (var pair in this) {
          return false;
        }

        return true;
      }
    }

    /**
     * @brief Determines the total number of unique entity IDs contained in this entity references collection.
     */
    public uint Count {
      get {
        var count = 0U;

        foreach (var pair in this) {
          count++;
        }

        return count;
      }
    }
    /// @}

    /// @name Operator Overloading
    /// @{
    public static EntityReferencesCollection operator +(EntityReferencesCollection entityReferencesCollection1, EntityReferencesCollection entityReferencesCollection2) {
      return entityReferencesCollection1.Merge(entityReferencesCollection2);
    }

    public static EntityReferencesCollection operator -(EntityReferencesCollection entityReferencesCollection1, EntityReferencesCollection entityReferencesCollection2) {
      return entityReferencesCollection1.RemoveEntityIds(entityReferencesCollection2);
    }

    public bool Equals(EntityReferencesCollection entityReferencesCollection) {
      var entityReferencesCollection1 = this;
      var entityReferencesCollection2 = entityReferencesCollection;

      if (entityReferencesCollection1.Count != entityReferencesCollection2.Count) {
        return false;
      }

      foreach (var pair in entityReferencesCollection1) {
        var entityType = pair.Key;
        var id = pair.Value;

        if (!entityReferencesCollection2.HasEntityId(entityType, id)) return false;
      }

      return true;
    }
    /// @}

    /// @name Enumeration
    /// @{
    public IEnumerator<KeyValuePair<string, uint>> GetEnumerator() {
      return
        this.GetEntityTypes()
        .SelectMany(entityType =>
          this.GetEntityIdsOfType(entityType)
          .Select(id =>
            new KeyValuePair<string, uint>(entityType, id)
          )
        )
        .GetEnumerator();
    }

    private IEnumerator GetGenericEnumerator() {
        return this.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return GetGenericEnumerator();
    }
    /// @}
  }
}