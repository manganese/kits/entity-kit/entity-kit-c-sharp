using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using UnityEngine;


namespace Manganese.EntityKit {
  /**
   * @brief An update of entities, including both upserted and removed entities.
   */
  [CreateAssetMenu(
    fileName = "Entities Update",
    menuName = "Manganese/EntityKit/Entities Update",
    order = 3
  )]
  [JsonConverter(typeof(EntitiesUpdateConverter))]
  public class EntitiesUpdate : ScriptableObject, IEquatable<EntitiesUpdate> {
    /// @name Constructors
    /// @{
    /**
     * @brief Create a new, empty entities update.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntitiesUpdate();
     * ```
     */
    public EntitiesUpdate() {}

    public EntitiesUpdate(EntitiesCollection entitiesCollection) {
      this.UnsafeSetUpdatedEntities(entitiesCollection);
    }

    /**
     * @brief Create a new entities update and copy the data from an existing instance.
     *
     * @param entitiesUpdate  The entities update from which to copy data.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate1 = new EntitiesUpdate().PutUpdatedEntity("widgets", widget1);
     * var entitiesUpdate2 = new EntitiesUpdate(entities1);
     * entities2.GetUpdatedEntity<Widget>("widgets", 1); // widget1
     * ```
     */
    public EntitiesUpdate(EntitiesUpdate entitiesUpdate) {
      this.UnsafeSetUpdatedEntities(entitiesUpdate.UpdatedEntities);
      this.UnsafeSetRemovedEntities(entitiesUpdate.RemovedEntities);
    }
    /// @}

    // Private state
    private EntitiesCollection updatedEntities = new EntitiesCollection();
    private EntityReferencesCollection removedEntities = new EntityReferencesCollection();

    /// @name Unsafe
    /// @{
    /**
     * @brief Get the internal entities collection used by the entities update to represent updated entities.
     *
     * @warning Note that the reference is preserved so modifying the value returned by this method can also affect the entities update.
     */
    public EntitiesCollection UnsafeGetUpdatedEntities() {
      return this.updatedEntities;
    }

    /**
     * @brief Overwrite the internal entities collection used by the entities update to represent updated entities.
     *
     * @warning The internal collection of updated entities is modified.
     *
     * @param updatedEntities  The new updated entities collection to set.
     */
    public void UnsafeSetUpdatedEntities(EntitiesCollection updatedEntities) {
      this.updatedEntities = updatedEntities;
    }

    /**
     * @brief Upsert an entity of a given type in the collection of updated entities.
     *
     * @warning The internal collection of updated entities is modified.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntitiesUpdate();
     * entitiesUpdate.UnsafePutUpdatedEntity("widgets", widget1);
     * entitiesUpdate.GetUpdatedEntity<Widget>("widgets", 1); // widget1
     * ```
     *
     * @param entityType  The entity type of the updated entity to put.
     * @param entity  The updated entity to put.
     */
    public void UnsafePutUpdatedEntity(string entityType, IEntity entity) {
      this.updatedEntities.UnsafePutEntity(entityType, entity);
    }

    /**
     * @brief Remove an entity of a given type by its ID from the collection of updated entities.
     *
     * @warning The internal collection of updated entities is modified.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntitiesUpdate().PutUpdatedEntity("widgets", widget1);
     * entitiesUpdate.GetUpdatedEntity<Widget>("widgets", 1); // widget1
     * entitiesUpdate.UnsafeRemoveUpdatedEntity("widgets", 1);
     * entitiesUpdate.GetUpdatedEntity<Widget>("widgets", 1); // null
     * ```
     *
     * @param entityType  The entity type of the updated entity to remove.
     * @param entity  The ID of the updated entity to remove.
     */
    public void UnsafeRemoveUpdatedEntity(string entityType, uint id) {
      this.updatedEntities.UnsafeRemoveEntity(entityType, id);
    }

    /**
     * @brief Get the internal entity references collection used by the entities update to represent removed entities.
     *
     * @warning Note that the reference is preserved so modifying the value returned by this method can also affect the entities update.
     */
    public EntityReferencesCollection UnsafeGetRemovedEntities() {
      return this.removedEntities;
    }

    /**
     * @brief Overwrite the internal entity references collection used by the entities update to represent removed entities.
     *
     * @warning The internal collection of removed entities is modified.
     *
     * @param removedEntities  The new removed entity references collection to set.
     */
    public void UnsafeSetRemovedEntities(EntityReferencesCollection removedEntities) {
      this.removedEntities = removedEntities;
    }

    /**
     * @brief Upsert a removed entity ID of a given entity type into this entities update.  The internal collection of removed entities is modified.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntityReferencesCollection();
     * entitiesUpdate.UnsafePutRemovedEntityId("widgets", 1);
     * entitiesUpdate.HasRemovedEntityId("widgets", 1); // true
     * ```
     *
     * @param entityType  The entity type of the removed entity ID to put.
     * @param id  The removed entity ID to put.
     */
    public void UnsafePutRemovedEntityId(string entityType, uint id) {
      this.removedEntities.UnsafePutEntityId(entityType, id);
    }

    /**
     * @brief Remove a removed entity ID of a given entity type from this entities update.  The internal collection of removed entities is modified.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntityReferencesCollection().PutRemovedEntityId("widgets", 1);
     * entitiesUpdate.HasRemovedEntityId("widgets", 1); // widget1
     * entitiesUpdate.UnsafeRemoveEntityId("widgets", 1);
     * entitiesUpdate.HasRemovedEntityId("widgets", 1); // null
     * ```
     *
     * @param entityType  The entity type of the removed entity to remove.
     * @param id  The removed entity ID to remove.
     */
    public void UnsafeRemoveRemovedEntityId(string entityType, uint id) {
      this.removedEntities.UnsafeRemoveEntityId(entityType, id);
    }
    /// @}

    /// @name Access
    /// @{
    /**
     * @brief Get the types of all modified entities in this entities updates, including both upserted and removed entities.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate =
     *   new EntitiesUpdate()
     *   .PutUpdatedEntity("widgets", widget)
     *   .PutRemovedEntityId("thingies", thingy.id);
     * entitiesUpdate.GetEntityTypes(); // HashSet<string> { "widgets", "thingies" }
     * ```
     */
    public HashSet<string> GetEntityTypes() {
      return
        new HashSet<string>(
          new HashSet<string>()
          .Union(this.updatedEntities.GetEntityTypes())
          .Union(this.removedEntities.GetEntityTypes())
        );
    }

    /**
     * @brief A copy of the collection of updated entities in this entities update.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntitiesUpdate().PutUpdatedEntity("widgets", widget);
     * entitiesUpdate.UpdatedEntities; // EntitiesCollection { { "widgets", widget } }
     * ```
     */
    public EntitiesCollection UpdatedEntities {
      get {
        return new EntitiesCollection(this.updatedEntities);
      }
    }

    /**
     * @brief Get the types of updated entities contained in this entities update, not including removed entities.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate =
     *   new EntitiesUpdate()
     *   .PutUpdatedEntity("widgets", widget)
     *   .PutRemovedEntityId("thingies", thingy.id);
     * entitiesUpdate.GetUpdatedEntityTypes(); // HashSet<string> { "widgets" }
     * ```
     */
    public HashSet<string> GetUpdatedEntityTypes() {
      return this.updatedEntities.GetEntityTypes();
    }

    /**
     * @brief Get a single updated entity of a given entity type by its ID from this entities update.
     *
     * @tparam EntityType  The type of the updated entity which is returned.
     *
     * @param entityType  The updated entity type of the entity to get.
     * @param id  The ID of the updated entity to get.
     */
    public EntityType GetUpdatedEntity<EntityType>(string entityType, uint id) where EntityType : IEntity {
      return this.updatedEntities.GetEntity<EntityType>(entityType, id);
    }

    /**
     * @brief Get all updated entities of a given entity type from this entities update.
     *
     * @tparam EntityType  The type of the updated entities which are returned.
     *
     * @param entityType  The entity type of the updated entities to get.
     */
    public Dictionary<uint, EntityType> GetUpdatedEntitiesOfType<EntityType>(string entityType) where EntityType : IEntity {
      return this.updatedEntities.GetEntitiesOfType<EntityType>(entityType);
    }

    /**
     * @brief Check if an entity of a given type with the given ID is present in the collection of updated entities in this entities update.
     *
     * @param entityType  The entity type of the updated entity to check for.
     * @param id  The ID of the updated entity to check for.
     */
    public bool HasUpdatedEntity(string entityType, uint id) {
      return this.updatedEntities.HasEntity(entityType, id);
    }

    /**
     * @brief A copy of the collection of removed entities in this entities update.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate = new EntitiesUpdate().PutRemovedEntityId("widgets", widget.id);
     * entitiesUpdate.RemovedEntities; // EntityReferencesCollection { { "widgets", widget.id } }
     * ```
     */
    public EntityReferencesCollection RemovedEntities {
      get {
        return new EntityReferencesCollection(this.removedEntities);
      }
    }

    /**
     * @brief Get the types of removed entities contained in this entities update, not including updated entities.
     *
     * Example:
     *
     * ```csharp
     * var entitiesUpdate =
     *   new EntitiesUpdate()
     *   .PutUpdatedEntity("widgets", widget)
     *   .PutRemovedEntityId("thingies", thingy.id);
     * entitiesUpdate.GetRemovedEntityTypes(); // HashSet<string> { "thingies" }
     * ```
     */
    public HashSet<string> GetRemovedEntityTypes() {
      return this.removedEntities.GetEntityTypes();
    }

    /**
     * @brief Check if a removed entity ID of a given entity type is contained in this entities update.
     *
     * @param entityType  The entity type of the removed entity to check for.
     * @param id  The removed entity ID to check for.
     */
    public bool HasRemovedEntityId(string entityType, uint id) {
      return this.removedEntities.HasEntityId(entityType, id);
    }

    /**
     * @brief Get all removed entity IDs of a given entity type from this entities update.
     *
     * @param entityType  The entity type of the removed entity IDs to get.
     */
    public HashSet<uint> GetRemovedEntityIdsOfType(string entityType) {
      return this.removedEntities.GetEntityIdsOfType(entityType);
    }
    /// @}

    /// @name Functional Mutation
    /// @{
    /**
     * @brief Put a single updated entity of a given entity type into a copy of this entities update.
     *
     * @tparam EntityType  The type of the updated entity to put.
     *
     * @param entityType  The entity type of the updated entity to put.
     * @param entity  The updated entity to put.
     */
    public EntitiesUpdate PutUpdatedEntity(string entityType, IEntity entity) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .PutEntity(entityType, entity)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Put multiple updated entities of a given entity type into a copy of this entities update.
     *
     * @tparam EntityType  The type of the entities to put.
     *
     * @param entityType  The entity type of the updated entities to put.
     * @param entitiesOfType  The updated entities of the given type to put.
     */
    public EntitiesUpdate PutUpdatedEntitiesOfType(string entityType, HashSet<IEntity> entitiesOfType) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .PutEntitiesOfType(entityType, entitiesOfType)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Put updated entities of multiple entity types into a copy of this entities update.
     *
     * @param entities  A dictionary of entity types to sets of updated entities to put.
     */
    public EntitiesUpdate PutUpdatedEntities(Dictionary<string, HashSet<IEntity>> entities) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .PutEntities(entities)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Upsert the updated entities contained in an entities collection into a copy of this entities update.
     *
     * @param entitiesCollection  A collection of updated entities to put.
     */
    public EntitiesUpdate PutUpdatedEntities(EntitiesCollection entitiesCollection) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .Merge(entitiesCollection)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove a single updated entity of a given entity type by its ID from a copy of this entities update.
     *
     * @param entityType  The entity type of the updated entity to remove.
     * @param id  The ID of the updated entity to remove.
     */
    public EntitiesUpdate RemoveUpdatedEntity(string entityType, uint id) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .RemoveEntity(entityType, id)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove multiple updated entities of a given entity type from a copy of this entities update.
     *
     * @param entityType  The entity type of the updated entities to remove.
     * @param ids  The IDs of the updated entities of the given type to remove.
     */
    public EntitiesUpdate RemoveUpdatedEntitiesOfType(string entityType, HashSet<uint> ids) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .RemoveEntitiesOfType(entityType, ids)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove updated entities which are referenced by a given entity references collection from a copy of this entities update.
     *
     * @param entityReferencesCollection  The entity references collection of IDs of updated entities to remove.
     */
    public EntitiesUpdate RemoveUpdatedEntities(EntityReferencesCollection entityReferencesCollection) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetUpdatedEntities(
        updatedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .RemoveEntities(entityReferencesCollection)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Put a single removed entity ID of a given entity type into a copy of this entities update.
     *
     * @param entityType  The entity type of the removed entity ID to put.
     * @param id  The removed entity ID to put.
     */
    public EntitiesUpdate PutRemovedEntityId(string entityType, uint id) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .PutEntityId(entityType, id)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Put multiple entity IDs of a given entity type into a copy of this entities update.
     *
     * @param entityType  The entity type of the entity IDs to put.
     * @param ids  The removed entity IDs of the given type to put.
     */
    public EntitiesUpdate PutRemovedEntityIdsOfType(string entityType, HashSet<uint> ids) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .PutEntityIdsOfType(entityType, ids)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Put entity IDs of multiple entity types into a copy of this entities update.
     *
     * @param ids  A dictionary of entity types to sets of removed entity IDs to put.
     */
    public EntitiesUpdate PutRemovedEntityIds(Dictionary<string, HashSet<uint>> ids) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .PutEntityIds(ids)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove a single removed entity ID of a given entity type from a copy of this entities update.
     *
     * @param entityType  The entity type of the removed entity ID to remove.
     * @param id  The removed entity ID of the given type to remove.
     */
    public EntitiesUpdate RemoveRemovedEntityId(string entityType, uint id) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .RemoveEntityId(entityType, id)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove multiple removed entity IDs of a given entity type from a copy of this entities update.
     *
     * @param entityType  The entity type of the removed entity IDs to remove.
     * @param ids  The removed entity IDs of the given type to remove.
     */
    public EntitiesUpdate RemoveRemovedEntityIdsOfType(string entityType, HashSet<uint> ids) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .RemoveEntityIdsOfType(entityType, ids)
      );

      return updatedEntitiesUpdate;
    }

    /**
     * @brief Remove removed entity IDs which are referenced by a given entity references collection from a copy of this entities update.
     *
     * @param entityReferencesCollection  The entity references collection of removed entity IDs to remove.
     */
    public EntitiesUpdate RemoveRemovedEntityIds(EntityReferencesCollection entityReferencesCollection) {
      var updatedEntitiesUpdate = new EntitiesUpdate(this);
      updatedEntitiesUpdate.UnsafeSetRemovedEntities(
        updatedEntitiesUpdate.UnsafeGetRemovedEntities()
        .RemoveEntityIds(entityReferencesCollection)
      );

      return updatedEntitiesUpdate;
    }
    /// @}

    /// @name Miscellaneous
    /// @{
    /***
     * @brief Merge another entities update into a copy of this entities update.
     *
     * @note If an updated entity of the same entity type and ID exists in both entities updates, the merged entities update will use values from the given entities update.
     *
     * @param entitiesUpdate  The entities update to merge with.
     */
    public EntitiesUpdate Merge(EntitiesUpdate entitiesUpdate) {
      var mergedEntitiesUpdate = new EntitiesUpdate(this);
      mergedEntitiesUpdate.UnsafeSetUpdatedEntities(
        mergedEntitiesUpdate.UnsafeGetUpdatedEntities()
        .Merge(entitiesUpdate.UnsafeGetUpdatedEntities())
      );
      mergedEntitiesUpdate.UnsafeSetRemovedEntities(
        mergedEntitiesUpdate.UnsafeGetRemovedEntities()
        .Merge(entitiesUpdate.UnsafeGetRemovedEntities())
      );

      return mergedEntitiesUpdate;
    }

    /***
     * @brief Determines whether at least one updated or removed entity of any type is contained in this entities update.
     */
    public bool Empty {
      get {
        return this.updatedEntities.Empty && this.removedEntities.Empty;
      }
    }
    /// @}

    /// @name Operator Overloading
    /// @{
    public static EntitiesUpdate operator +(EntitiesUpdate entitiesUpdate1, EntitiesUpdate entitiesUpdate2) {
      return entitiesUpdate1.Merge(entitiesUpdate2);
    }

    public static EntitiesUpdate operator +(EntitiesUpdate entitiesUpdate, EntitiesCollection entitiesCollection) {
      var addedEntitiesUpdate = new EntitiesUpdate(entitiesUpdate);
      addedEntitiesUpdate.UnsafeSetUpdatedEntities(
        addedEntitiesUpdate.UnsafeGetUpdatedEntities() + entitiesCollection
      );

      return addedEntitiesUpdate;
    }

    public static EntitiesUpdate operator -(EntitiesUpdate entitiesUpdate1, EntitiesUpdate entitiesUpdate2) {
      var subtractedEntitiesUpdate = new EntitiesUpdate(entitiesUpdate1);
      subtractedEntitiesUpdate.UnsafeSetUpdatedEntities(
        subtractedEntitiesUpdate.UnsafeGetUpdatedEntities() - (EntityReferencesCollection) entitiesUpdate2.UnsafeGetUpdatedEntities()
      );
      subtractedEntitiesUpdate.UnsafeSetRemovedEntities(
        subtractedEntitiesUpdate.UnsafeGetRemovedEntities() - entitiesUpdate2.UnsafeGetRemovedEntities()
      );

      return subtractedEntitiesUpdate;
    }

    public bool Equals(EntitiesUpdate entitiesUpdate) {
      var entitiesUpdate1 = this;
      var entitiesUpdate2 = entitiesUpdate;

      return (
        entitiesUpdate1.UnsafeGetUpdatedEntities().Equals(entitiesUpdate2.UnsafeGetUpdatedEntities()) &&
        entitiesUpdate1.UnsafeGetRemovedEntities().Equals(entitiesUpdate2.UnsafeGetRemovedEntities())
      );
    }
    /// @}
  }
}