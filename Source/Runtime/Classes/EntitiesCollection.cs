using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using UnityEngine;


namespace Manganese.EntityKit {
  /**
   * @brief A collection of entities referenced by entity type and ID.
   */
  [CreateAssetMenu(
    fileName = "Entities Collection",
    menuName = "Manganese/EntityKit/Entities Collection",
    order = 1
  )]
  [JsonConverter(typeof(EntitiesCollectionConverter))]
  public class EntitiesCollection : ScriptableObject, IEnumerable<KeyValuePair<string, IEntity>>, IEquatable<EntitiesCollection> {
    /// @name Constructors
    /// @{
    /**
     * @brief Create a new, empty entities collection.
     *
     * Example:
     *
     * ```csharp
     * var entities = new EntitiesCollection();
     * ```
     */
    public EntitiesCollection() {}

    /**
     * @brief Create a new entities collection and copy the data from an existing instance.
     *
     * @param entitiesCollection  The entities collection from which to copy data.
     *
     * Example:
     *
     * ```csharp
     * var entities1 = new EntitiesCollection().PutEntity("widgets", widget1);
     * var entities2 = new EntitiesCollection(entities1);
     * entities2.GetEntity<Widget>("widgets", 1); // widget1
     * ```
     */
    public EntitiesCollection(EntitiesCollection entitiesCollection) {
      foreach (var pair in entitiesCollection) {
        this.UnsafePutEntity(pair.Key, pair.Value);
      }
    }
    /// @}

    // Private State
    private Dictionary<string /* entity type */, Dictionary<uint /* entity ID */, IEntity>> entities = new Dictionary<string, Dictionary<uint, IEntity>>();

    /// @name Unsafe
    /// @{
    /**
     * @brief Get the internal entities data structure used by the entities collection.
     *
     * @warning Note that the reference is preserved so modifying the value returned by this method can also affect the entities collection.
     */
    public Dictionary<string, Dictionary<uint, IEntity>> UnsafeGetEntities() {
      return this.entities;
    }

    /**
     * @brief Overwrite the internal entities data structure used by the entities collection.
     *
     * @warning The internal entities data structure is modified.
     *
     * @param entities  The new entities data to set.
     */
    public void UnsafeSetEntities(Dictionary<string, Dictionary<uint, IEntity>> entities) {
      this.entities = entities;
    }

    /**
     * @brief Upsert an entity of a given type in the entities collection.
     *
     * @warning The internal entities data structure is modified.
     *
     * Example:
     *
     * ```csharp
     * var entities = new EntitiesCollection();
     * entities.UnsafePutEntity("widgets", widget1);
     * entities.GetEntity<Widget>("widgets", 1); // widget1
     * ```
     *
     * @param entityType  The entity type of the entity to put.
     * @param entity  The entity to put.
     */
    public void UnsafePutEntity(string entityType, IEntity entity) {
      if (!this.entities.ContainsKey(entityType)) {
        this.entities[entityType] = new Dictionary<uint, IEntity>();
      }

      var entitiesOfType = this.entities[entityType];
      entitiesOfType[entity.id] = entity;
    }

    /**
     * @brief Remove an entity of a given type by its ID from the entities collection.
     *
     * @warning The internal entities data structure is modified.
     *
     * Example:
     *
     * ```csharp
     * var entities = new EntitiesCollection().PutEntity("widgets", widget1);
     * entities.GetEntity<Widget>("widgets", 1); // widget1
     * entities.UnsafeRemoveEntity("widgets", 1);
     * entities.GetEntity<Widget>("widgets", 1); // null
     * ```
     *
     * @param entityType  The entity type of the entity to remove.
     * @param entity  The ID of the entity to remove.
     * @correlationId{unsafe-remove-entity}
     */
    public void UnsafeRemoveEntity(string entityType, uint id) {
      var entitiesOfType = this.entities[entityType];

      if (entitiesOfType == null) {
        return;
      }

      entitiesOfType.Remove(id);

      if (entitiesOfType.Count == 0) {
        this.entities.Remove(entityType);
      }
    }
    /// @}

    /// @name Access
    /// @{
    /**
     * @brief Get the types of entities contained in this entities collection.
     *
     * Example:
     *
     * ```csharp
     * var entities =
     *   new EntitiesCollection()
     *   .PutEntity("widgets", widget)
     *   .PutEntity("thingies", thingy);
     * entities.GetEntityTypes(); // HashSet<string> { "widgets", "thingies" }
     * ```
     */
    public HashSet<string> GetEntityTypes() {
      return new HashSet<string>(this.entities.Keys);
    }

    /**
     * @brief Check if an entity of a given type with the given ID is present in the entities collection.
     *
     * @param entityType  The entity type of the entity to check for.
     * @param id  The ID of the entity to check for.
     */
    public bool HasEntity(string entityType, uint id) {
      if (!this.entities.ContainsKey(entityType)) {
        return false;
      }

      return this.entities[entityType].ContainsKey(id);
    }

    /**
     * @brief Get a single entity of a given entity type by its ID from the entities collection.
     *
     * @tparam EntityType  The type of the entity which is returned.
     *
     * @param entityType  The entity type of the entity to get.
     * @param id  The ID of the entity to get.
     */
    public EntityType GetEntity<EntityType>(string entityType, uint id) where EntityType : IEntity {
      if (!this.entities.ContainsKey(entityType)) {
        return default (EntityType);
      }

      var entitiesOfType = this.entities[entityType];

      if (!entitiesOfType.ContainsKey(id)) {
        return default (EntityType);
      }

      return (EntityType) entitiesOfType[id];
    }

    /**
     * @brief Get all entities of a given entity type from the entities collection.
     *
     * @tparam EntityType  The type of the entities which are returned.
     *
     * @param entityType  The entity type of the entities to get.
     */
    public Dictionary<uint, EntityType> GetEntitiesOfType<EntityType>(string entityType) where EntityType : IEntity {
      if (!this.entities.ContainsKey(entityType)) {
        return new Dictionary<uint, EntityType>();
      }

      var entitiesOfType = new Dictionary<uint, EntityType>();

      foreach (var entry in this.entities[entityType]) {
        entitiesOfType[entry.Key] = (EntityType) entry.Value;
      }

      return entitiesOfType;
    }

    /// @}

    /// @name Functional Mutation
    /// @{
    /**
     * @brief Put a single entity of a given entity type into a copy of the entities collection.
     *
     * @tparam EntityType  The type of the entity to put.
     *
     * @param entityType  The entity type of the entity to put.
     * @param entity  The entity to put.
     */
    public EntitiesCollection PutEntity(string entityType, IEntity entity) {
      var entitiesCollection = new EntitiesCollection(this);
      entitiesCollection.UnsafePutEntity(entityType, entity);

      return entitiesCollection;
    }

    /**
     * @brief Put multiple entities of a given entity type into a copy of the entities collection.
     *
     * @tparam EntityType  The type of the entities to put.
     *
     * @param entityType  The entity type of the entities to put.
     * @param entitiesOfType  The entities to put.
     */
    public EntitiesCollection PutEntitiesOfType(string entityType, HashSet<IEntity> entitiesOfType) {
      var entitiesCollection = new EntitiesCollection(this);

      foreach (var entity in entitiesOfType) {
        entitiesCollection.UnsafePutEntity(entityType, entity);
      }

      return entitiesCollection;
    }

    /**
     * @brief Put entities of multiple entity types into a copy of the entities collection.
     *
     * @param entities  A dictionary of entity types to sets of entities to put.
     */
    public EntitiesCollection PutEntities(Dictionary<string, HashSet<IEntity>> entities) {
      var entitiesCollection = new EntitiesCollection(this);

      foreach (var entityType in entities.Keys) {
        var entitiesOfType = entities[entityType];

        foreach (var entity in entitiesOfType) {
          entitiesCollection.UnsafePutEntity(entityType, entity);
        }
      }

      return entitiesCollection;
    }

    /**
     * @brief Remove a single entity of a given entity type by its ID from a copy of this entities collection.
     *
     * @param entityType  The entity type of the entity to remove.
     * @param id  The ID of the entity to remove.
     */
    public EntitiesCollection RemoveEntity(string entityType, uint id) {
      var entitiesCollection = new EntitiesCollection(this);
      entitiesCollection.UnsafeRemoveEntity(entityType, id);

      return entitiesCollection;
    }

    /**
     * @brief Remove multiple entities of a given entity type from a copy of this entities collection.
     *
     * @param entityType  The entity type of the entities to remove.
     * @param ids  The IDs of the entities of the given type to remove.
     */
    public EntitiesCollection RemoveEntitiesOfType(string entityType, HashSet<uint> ids) {
      var entitiesCollection = new EntitiesCollection(this);

      foreach (var id in ids) {
        entitiesCollection.UnsafeRemoveEntity(entityType, id);
      }

      return entitiesCollection;
    }

    /**
     * @brief Remove entities which are referenced by a given entity references collection from a copy of this entities collection.
     *
     * @param entityReferencesCollection  The entity references collection of entity IDs to remove.
     */
    public EntitiesCollection RemoveEntities(EntityReferencesCollection entityReferencesCollection) {
      var entitiesCollection = new EntitiesCollection(this);

      foreach (var pair in entityReferencesCollection) {
        entitiesCollection.UnsafeRemoveEntity(pair.Key, pair.Value);
      }

      return entitiesCollection;
    }

    /**
     * @brief Apply an entities update to this entities collection, both upserting and removing entities.
     *
     * @param entitiesUpdate  The entities update to apply.
     */
    public EntitiesCollection ApplyUpdate(EntitiesUpdate entitiesUpdate) {
      var updatedEntitiesCollection =
        new EntitiesCollection(this)
        .Merge(entitiesUpdate.UpdatedEntities)
        .RemoveEntities(entitiesUpdate.RemovedEntities);

      return updatedEntitiesCollection;
    }
    /// @}

    /// @name Miscellaneous
    /// @{
    /***
     * @brief Merge another entities collection into a copy of this entities collection.
     *
     * @note If an entity of the same entity type and ID exists in both entities collections, the merged entities collection will use values from the given entities collection.
     *
     * @param entitiesCollection  The entities collection to merge with.
     */
    public EntitiesCollection Merge(EntitiesCollection entitiesCollection) {
      var mergedEntitiesCollection = new EntitiesCollection(this);

      foreach (var pair in entitiesCollection) {
        mergedEntitiesCollection.UnsafePutEntity(pair.Key, pair.Value);
      }

      return mergedEntitiesCollection;
    }

    /***
     * @brief Determines whether at least one entity of any type is contained in the entities collection.
     */
    public bool Empty {
      get {
        foreach (var pair in this) {
          return false;
        }

        return true;
      }
    }

    /**
     * @brief Determines the total number of unique entities contained in the entities collection.
     */
    public uint Count {
      get {
        var count = 0U;

        foreach (var pair in this) {
          count++;
        }

        return count;
      }
    }
    /// @}

    /// @name Casting
    /// @{
    public static explicit operator EntityReferencesCollection(EntitiesCollection entitiesCollection) {
      var entityReferencesCollection = new EntityReferencesCollection();

      foreach (var pair in entitiesCollection) {
        entityReferencesCollection.UnsafePutEntityId(pair.Key, pair.Value.id);
      }

      return entityReferencesCollection;
    }
    /// @}

    /// @name Operator Overloading
    /// @{
    public static EntitiesCollection operator +(EntitiesCollection entitiesCollection1, EntitiesCollection entitiesCollection2) {
      return entitiesCollection1.Merge(entitiesCollection2);
    }

    public static EntitiesCollection operator -(EntitiesCollection entitiesCollection, EntityReferencesCollection entityReferencesCollection) {
      return entitiesCollection.RemoveEntities(entityReferencesCollection);
    }

    public bool Equals(EntitiesCollection entitiesCollection) {
      var entitiesCollection1 = this;
      var entitiesCollection2 = entitiesCollection;

      if (entitiesCollection1.Count != entitiesCollection2.Count) {
        return false;
      }

      foreach (var pair in entitiesCollection1) {
        var entityType = pair.Key;
        var entity1 = pair.Value;
        var entity2 = default(IEntity);

        if (!entitiesCollection2.GetEntitiesOfType<IEntity>(entityType).TryGetValue(entity1.id, out entity2)) return false;
        if (!entity1.Equals(entity2)) return false;
      }

      return true;
    }
    /// @}

    /// @name Enumeration
    /// @{
    public IEnumerator<KeyValuePair<string, IEntity>> GetEnumerator() {
      return
        this.GetEntityTypes()
        .SelectMany(entityType =>
          this.GetEntitiesOfType<IEntity>(entityType)
          .Select(pair =>
            new KeyValuePair<string, IEntity>(entityType, pair.Value)
          )
        )
        .GetEnumerator();
    }

    private IEnumerator GetGenericEnumerator() {
        return this.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return GetGenericEnumerator();
    }
    /// @}
  }
}