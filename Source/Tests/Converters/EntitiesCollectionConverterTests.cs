using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.EntityKit;
using Manganese.EntityKit.Tests.Support;

namespace Manganese.EntityKit.Converters {
    public class EntitiesCollectionConverterTests : EntityKitTests {
        [Test]
        public void Serialize() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });

            Assert.AreEqual(
                JsonConvert.SerializeObject(entitiesCollection, Formatting.Indented),
                @"{
  ""entities"": {
    """ + WIDGETS_ENTITY_TYPE + @""": {
      ""1"": {
        ""name"": ""widget1"",
        ""id"": 1
      },
      ""2"": {
        ""name"": ""widget2"",
        ""id"": 2
      }
    }
  }
}"
            );
        }

        [Test]
        public void Deserialize() {
            EntityRegistrar.RegisterEntityType(WIDGETS_ENTITY_TYPE, typeof(Widget));

            var entitiesCollection =
                JsonConvert.DeserializeObject<EntitiesCollection>(@"{
                  ""entities"": {
                    """ + WIDGETS_ENTITY_TYPE + @""": {
                      ""1"": {
                        ""id"": 1,
                        ""name"": ""widget1""
                      },
                      ""2"": {
                        ""id"": 2,
                        ""name"": ""widget2""
                      }
                    }
                  }
                }");

            var correctEntitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });

            Assert.AreEqual(
                entitiesCollection,
                correctEntitiesCollection
            );
        }
    }
}
