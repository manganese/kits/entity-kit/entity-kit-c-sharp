using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.EntityKit;
using Manganese.EntityKit.Tests.Support;

namespace Manganese.EntityKit.Converters {
    public class EntityReferencesCollectionConverterTests : EntityKitTests {
        [Test]
        public void Serialize() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.AreEqual(
                JsonConvert.SerializeObject(entityReferencesCollection, Formatting.Indented),
                @"{
  ""entityIds"": {
    """ + WIDGETS_ENTITY_TYPE + @""": [
      " + WIDGET1.id + @",
      " + WIDGET2.id + @"
    ]
  }
}"
            );
        }

        [Test]
        public void Deserialize() {
            var entityReferencesCollection =
                JsonConvert.DeserializeObject<EntityReferencesCollection>(@"{
                  ""entityIds"": {
                    """ + WIDGETS_ENTITY_TYPE + @""": [
                      " + WIDGET1.id + @",
                      " + WIDGET2.id + @"
                    ]
                  }
                }");

            var correctEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.AreEqual(
                entityReferencesCollection,
                correctEntityReferencesCollection
            );
        }
    }
}
