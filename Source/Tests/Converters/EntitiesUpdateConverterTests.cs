using System.Collections;
using System.Collections.Generic;

using Newtonsoft.Json;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.EntityKit;
using Manganese.EntityKit.Tests.Support;

namespace Manganese.EntityKit.Converters {
    public class EntitiesUpdateConverterTests : EntityKitTests {
        [Test]
        public void Serialize() {
            var entitiesUpdate =
                new EntitiesUpdate()
                .PutUpdatedEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutRemovedEntityIdsOfType(THINGIES_ENTITY_TYPE, new HashSet<uint>() {
                    THINGY1.id,
                    THINGY2.id
                });

            Assert.AreEqual(
                JsonConvert.SerializeObject(entitiesUpdate, Formatting.Indented),
                @"{
  ""updatedEntities"": {
    ""entities"": {
      """ + WIDGETS_ENTITY_TYPE + @""": {
        """ + WIDGET1.id + @""": {
          ""name"": ""widget1"",
          ""id"": 1
        }
      }
    }
  },
  ""removedEntities"": {
    ""entityIds"": {
      """ + THINGIES_ENTITY_TYPE + @""": [
        " + THINGY1.id + @",
        " + THINGY2.id + @"
      ]
    }
  }
}"
            );
        }

        [Test]
        public void Deserialize() {
            EntityRegistrar.RegisterEntityType(WIDGETS_ENTITY_TYPE, typeof(Widget));
            EntityRegistrar.RegisterEntityType(THINGIES_ENTITY_TYPE, typeof(Thingy));

            var entitiesUpdate =
                JsonConvert.DeserializeObject<EntitiesUpdate>(@"{
                  ""updatedEntities"": {
                    ""entities"": {
                      """ + WIDGETS_ENTITY_TYPE + @""": {
                        """ + WIDGET1.id + @""": {
                          ""id"": 1,
                          ""name"": ""widget1""
                        }
                      }
                    }
                  },
                  ""removedEntities"": {
                    ""entityIds"": {
                      """ + THINGIES_ENTITY_TYPE + @""": [
                        " + THINGY1.id + @",
                        " + THINGY2.id + @"
                      ]
                    }
                  }
                }");

            var correctEntitiesUpdate =
                new EntitiesUpdate()
                .PutUpdatedEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutRemovedEntityIdsOfType(THINGIES_ENTITY_TYPE, new HashSet<uint>() {
                    THINGY1.id,
                    THINGY2.id
                });

            Assert.AreEqual(
                entitiesUpdate,
                correctEntitiesUpdate
            );
        }
    }
}
