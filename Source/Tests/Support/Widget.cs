using System;

namespace Manganese.EntityKit.Tests.Support {
  public class Widget : MockEntity, IEquatable<Widget> {
    public Widget(uint id, string name) : base(id) {
      this.name = name;
    }

    public string name { get; set; }

    public override bool Equals(object widget) {
      try {
        return Equals((Widget) widget);
      } catch(Exception exception) {
        return false;
      }
    }

    public bool Equals(Widget widget) {
      return (
        this.id == widget.id &&
        this.name == widget.name
      );
    }
  }
}