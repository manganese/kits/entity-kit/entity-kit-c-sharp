using System;

using Manganese.EntityKit;

namespace Manganese.EntityKit.Tests.Support {
  public class MockEntity : IEntity {
    public MockEntity(uint id) {
      this.id = id;
    }

    public uint id { get; set; }
  }
}