using System;

namespace Manganese.EntityKit.Tests.Support {
  public class Thingy : MockEntity, IEquatable<Thingy> {
    public Thingy(uint id, string key) : base(id) {
      this.key = key;
    }

    public string key { get; set; }

    public override bool Equals(object thingy) {
      try {
        return Equals((Thingy) thingy);
      } catch(Exception exception) {
        return false;
      }
    }

    public bool Equals(Thingy thingy) {
      return this.id == thingy.id &&
             this.key == thingy.key;
    }
  }
}