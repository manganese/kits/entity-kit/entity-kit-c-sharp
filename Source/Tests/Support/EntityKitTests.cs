

namespace Manganese.EntityKit.Tests.Support {
  public class EntityKitTests {
    // Widgets
    protected static string WIDGETS_ENTITY_TYPE = "widgets";
    protected static Widget WIDGET1 {
        get {
            return new Widget(1, "widget1");
        }
    }

    protected static Widget WIDGET2 {
        get {
            return new Widget(2, "widget2");
        }
    }

    // Thingies
    protected static string THINGIES_ENTITY_TYPE = "thingies";
    protected static Thingy THINGY1 {
        get {
            return new Thingy(1, "thingy1");
        }
    }

    protected static Thingy THINGY2 {
        get {
            return new Thingy(2, "thingy2");
        }
    }
  }
}