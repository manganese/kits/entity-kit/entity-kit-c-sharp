using System.Linq;
using System.Collections;
using System.Collections.Generic;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.EntityKit;
using Manganese.EntityKit.Tests.Support;

namespace Manganese.EntityKit.Classes {
    public class EntityReferencesCollectionTests : EntityKitTests {
        [Test]
        public void ConstructorWhenCalledWithAnEntityReferencesCollectionArgumentCopiesFromTheExistingEntityReferencesCollection() {
            var originalEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);
            var entityReferencesCollection = new EntityReferencesCollection(originalEntityReferencesCollection);

            Assert.IsTrue(entityReferencesCollection.HasEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void ConstructorWhenCalledWithNoArgumentCreatesAnEmptyEntityReferencesCollection() {
            var entityReferencesCollection = new EntityReferencesCollection();

            Assert.AreEqual(
                entityReferencesCollection.Count,
                0
            );
        }

        [Test]
        public void GetEntityTypesReturnsASetOfTheEntityTypes() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                })
                .PutEntityId(THINGIES_ENTITY_TYPE, THINGY1.id);

            Assert.AreEqual(
                entityReferencesCollection.GetEntityTypes(),
                new HashSet<string>() {
                    WIDGETS_ENTITY_TYPE,
                    THINGIES_ENTITY_TYPE
                }
            );
        }

        [Test]
        public void HasEntityIdWhenThereIsAnEntityIDOfTheGivenEntityTypeItReturnsTrue() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.IsTrue(entityReferencesCollection.HasEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void HasEntityIdWhenThereIsNoEntityIDOfTheGivenEntityTypeItReturnsFalse() {
            var entityReferencesCollection = new EntityReferencesCollection();

            Assert.IsFalse(entityReferencesCollection.HasEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void GetEntityIdsOfTypeWhenThereAreEntityIDsOfTheGivenEntityTypeReturnsAHashSetOfTheEntityIDs() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id)
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                }
            );
        }

        [Test]
        public void GetEntityIdsOfTypeWhenThereAreNoEntityIDsOfTheGivenEntityTypeReturnsAnEmptyHashSet() {
            var entityReferencesCollection = new EntityReferencesCollection();

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>()
            );
        }

        [Test]
        public void PutEntityIdReturnsANewEntityReferencesCollectionWithTheEntityIDUpserted() {
            var originalEntityReferencesCollection = new EntityReferencesCollection();
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.IsTrue(entityReferencesCollection.HasEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id));
            Assert.AreNotEqual(entityReferencesCollection, originalEntityReferencesCollection);
            Assert.IsFalse(originalEntityReferencesCollection.HasEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void PutEntityIdsOfTypeReturnsANewEntityReferencesCollectionWithTheEntityIDsUpserted() {
            var originalEntityReferencesCollection = new EntityReferencesCollection();
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                }
            );
            Assert.AreNotEqual(entityReferencesCollection, originalEntityReferencesCollection);
            Assert.AreEqual(
                originalEntityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>()
            );
        }

        [Test]
        public void PutEntityIdsReturnsANewEntityReferencesCollectionWithTheEntityIdsUpserted() {
            var originalEntityReferencesCollection = new EntityReferencesCollection();
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .PutEntityIds(new Dictionary<string, HashSet<uint>>() {
                    { WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                        WIDGET1.id,
                        WIDGET2.id
                    }}
                });

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                }
            );
            Assert.AreNotEqual(entityReferencesCollection, originalEntityReferencesCollection);
            Assert.AreEqual(
                originalEntityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>()
            );
        }

        [Test]
        public void RemoveEntityIdReturnsANewEntityReferencesCollectionWithTheEntityIDRemoved() {
            var originalEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .RemoveEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET2.id
                }
            );
            Assert.AreNotEqual(entityReferencesCollection, originalEntityReferencesCollection);
            Assert.AreEqual(
                originalEntityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                }
            );
        }

        [Test]
        public void RemoveEntityIdsOfTypeReturnsANewEntityReferencesCollectionWithTheEntityIDsRemoved() {
            var originalEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .RemoveEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.IsTrue(entityReferencesCollection.Empty);
            Assert.AreNotEqual(entityReferencesCollection, originalEntityReferencesCollection);
            Assert.AreEqual(
                originalEntityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                }
            );
        }

        [Test]
        public void RemoveEntityIdsReturnsANewEntityReferencesCollectionWithTheEntityIDsRemoved() {
            var entityReferencesCollectionToRemove =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var originalEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });
            var entityReferencesCollection =
                originalEntityReferencesCollection
                .RemoveEntityIds(entityReferencesCollectionToRemove);

            Assert.AreEqual(
                entityReferencesCollection.GetEntityIdsOfType(WIDGETS_ENTITY_TYPE),
                new HashSet<uint>() {
                    WIDGET1.id
                }
            );
        }

        [Test]
        public void MergeReturnsTheMergedEntityReferencesCollection() {
            var entityReferencesCollection1 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);
            var entityReferencesCollection2 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var mergedEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.AreEqual(
                entityReferencesCollection1.Merge(entityReferencesCollection2),
                mergedEntityReferencesCollection
            );
        }

        [Test]
        public void EmptyWhenTheEntityReferencesCollectionIsEmptyReturnsTrue() {
            var entityReferencesCollection = new EntityReferencesCollection();

            Assert.IsTrue(entityReferencesCollection.Empty);
        }

        [Test]
        public void EmptyWhenTheEntityReferencesCollectionIsNotEmptyReturnsFalse() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.IsFalse(entityReferencesCollection.Empty);
        }

        [Test]
        public void CountReturnsTheNumberOfEntityIDsInTheEntityReferencesCollection() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                })
                .PutEntityId(THINGIES_ENTITY_TYPE, THINGY1.id);

            Assert.AreEqual(
                entityReferencesCollection.Count,
                3
            );
        }

        [Test]
        public void PlusOperatorMergesTheOperandEntityReferencesCollections() {
            var entityReferencesCollection1 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);
            var entityReferencesCollection2 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var mergedEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.AreEqual(
                entityReferencesCollection1 + entityReferencesCollection2,
                mergedEntityReferencesCollection
            );
        }

        [Test]
        public void EqualsWhenTheEntityReferencesCollectionsAreEqualReturnsTrue() {
            var entityReferencesCollection1 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id)
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var entityReferencesCollection2 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id)
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);

            Assert.IsTrue(entityReferencesCollection1.Equals(entityReferencesCollection2));
        }

        [Test]
        public void EqualsWhenTheEntityReferencesCollectionsAreNotEqualReturnFalse() {
            var entityReferencesCollection1 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id)
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var entityReferencesCollection2 =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.IsFalse(entityReferencesCollection1.Equals(entityReferencesCollection2));
        }

        [Test]
        public void GetEnumeratorReturnsAnEnumeratorOverEachEntityByType() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                })
                .PutEntityId(THINGIES_ENTITY_TYPE, THINGY1.id);

            var pairs = (
                from pair in entityReferencesCollection
                select pair
            ).ToList();

            Assert.AreEqual(
                pairs,
                new List<KeyValuePair<string, uint>>() {
                    new KeyValuePair<string, uint>(WIDGETS_ENTITY_TYPE, WIDGET1.id),
                    new KeyValuePair<string, uint>(WIDGETS_ENTITY_TYPE, WIDGET2.id),
                    new KeyValuePair<string, uint>(THINGIES_ENTITY_TYPE, THINGY1.id)
                }
            );
        }
    }
}
