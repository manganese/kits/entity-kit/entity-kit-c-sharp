using System.Linq;
using System.Collections;
using System.Collections.Generic;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using Manganese.EntityKit;
using Manganese.EntityKit.Tests.Support;

namespace Manganese.EntityKit.Classes {
    public class EntitiesCollectionTests : EntityKitTests {
        [Test]
        public void ConstructorWhenCalledWithAnEntitiesCollectionArgumentCopiesFromTheExistingEntitiesCollection() {
            var originalEntitiesCollection = new EntitiesCollection();
            originalEntitiesCollection.UnsafePutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            var entitiesCollection = new EntitiesCollection(originalEntitiesCollection);

            Assert.AreEqual(
                entitiesCollection.GetEntity<Widget>(WIDGETS_ENTITY_TYPE, WIDGET1.id),
                WIDGET1
            );
        }

        [Test]
        public void ConstructorWhenCalledWithNoArgumentCreatesAnEmptyEntitiesCollection() {
            var entitiesCollection = new EntitiesCollection();

            Assert.AreEqual(
                entitiesCollection.UnsafeGetEntities().Count,
                0
            );
        }

        [Test]
        public void GetEntityTypesReturnsASetOfTheEntityTypes() {
            var entitiesCollection = new EntitiesCollection();
            entitiesCollection.UnsafePutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);
            entitiesCollection.UnsafePutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);

            Assert.AreEqual(
                entitiesCollection.GetEntityTypes(),
                new HashSet<string>() {
                    WIDGETS_ENTITY_TYPE
                }
            );
        }

        [Test]
        public void HasEntityWhenThereIsAnEntityOfTheGivenEntityTypeAndIDItReturnsTrue() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            Assert.IsTrue(entitiesCollection.HasEntity(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void HasEntityWhenThereIsNoEntityOfTheGivenEntityTypeAndIDItReturnsFalse() {
            var entitiesCollection = new EntitiesCollection();

            Assert.IsFalse(entitiesCollection.HasEntity(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void GetEntityWhenThereIsAnEntityOfTheGivenEntityTypeAndIDItReturnsTheEntity() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            Assert.AreEqual(
                entitiesCollection.GetEntity<Widget>(WIDGETS_ENTITY_TYPE, WIDGET1.id),
                WIDGET1
            );
        }

        [Test]
        public void GetEntityWhenThereIsNoEntityOfTheGivenEntityTypeAndIDItReturnsNull() {
            var entitiesCollection = new EntitiesCollection();

            Assert.IsNull(entitiesCollection.GetEntity<Widget>(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void GetEntitiesOfTypeWhenThereAreEntitiesOfTheGivenEntityTypeReturnsADictionaryOfTheEntitiesByID() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 },
                    { WIDGET2.id, WIDGET2 }
                }
            );
        }

        [Test]
        public void GetEntitiesOfTypeWhenThereAreNoEntitiesOfTheGivenEntityTypeReturnsAnEmptyDictionary() {
            var entitiesCollection = new EntitiesCollection();

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>()
            );
        }

        [Test]
        public void PutEntityReturnsANewEntitiesCollectionWithTheEntityUpserted() {
            var originalEntitiesCollection = new EntitiesCollection();
            var entitiesCollection = originalEntitiesCollection.PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            Assert.AreEqual(
                entitiesCollection.GetEntity<Widget>(WIDGETS_ENTITY_TYPE, WIDGET1.id),
                WIDGET1
            );
            Assert.AreNotEqual(entitiesCollection, originalEntitiesCollection);
            Assert.IsNull(originalEntitiesCollection.GetEntity<Widget>(WIDGETS_ENTITY_TYPE, WIDGET1.id));
        }

        [Test]
        public void PutEntitiesOfTypeReturnsANewEntitiesCollectionWithTheEntitiesUpserted() {
            var originalEntitiesCollection = new EntitiesCollection();
            var entitiesCollection =
                originalEntitiesCollection
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 },
                    { WIDGET2.id, WIDGET2 }
                }
            );
            Assert.AreNotEqual(entitiesCollection, originalEntitiesCollection);
            Assert.AreEqual(
                originalEntitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>()
            );
        }

        [Test]
        public void PutEntitiesReturnsANewEntitiesCollectionWithTheEntitiesUpserted() {
            var originalEntitiesCollection = new EntitiesCollection();
            var entitiesCollection =
                originalEntitiesCollection
                .PutEntities(new Dictionary<string, HashSet<IEntity>>() {
                    { WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                        WIDGET1,
                        WIDGET2
                    }}
                });

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 },
                    { WIDGET2.id, WIDGET2 }
                }
            );
            Assert.AreNotEqual(entitiesCollection, originalEntitiesCollection);
            Assert.AreEqual(
                originalEntitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>()
            );
        }

        [Test]
        public void RemoveEntityReturnsANewEntitiesCollectionWithTheEntityRemoved() {
            var originalEntitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);
            var entitiesCollection =
                originalEntitiesCollection
                .RemoveEntity(WIDGETS_ENTITY_TYPE, WIDGET1.id);

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET2.id, WIDGET2 }
                }
            );
            Assert.AreNotEqual(entitiesCollection, originalEntitiesCollection);
            Assert.AreEqual(
                originalEntitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 },
                    { WIDGET2.id, WIDGET2 }
                }
            );
        }

        [Test]
        public void RemoveEntitiesOfTypeReturnsANewEntitiesCollectionWithTheEntitiesRemoved() {
            var originalEntitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });
            var entitiesCollection =
                originalEntitiesCollection
                .RemoveEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                });

            Assert.IsTrue(entitiesCollection.Empty);
            Assert.AreNotEqual(entitiesCollection, originalEntitiesCollection);
            Assert.AreEqual(
                originalEntitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 },
                    { WIDGET2.id, WIDGET2 }
                }
            );
        }

        [Test]
        public void RemoveEntitiesReturnsANewEntitiesCollectionWithTheEntitiesRemoved() {
            var entityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityId(WIDGETS_ENTITY_TYPE, WIDGET2.id);
            var originalEntitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });
            var entitiesCollection =
                originalEntitiesCollection
                .RemoveEntities(entityReferencesCollection);

            Assert.AreEqual(
                entitiesCollection.GetEntitiesOfType<Widget>(WIDGETS_ENTITY_TYPE),
                new Dictionary<uint, Widget>() {
                    { WIDGET1.id, WIDGET1 }
                }
            );
        }

        [Test]
        public void MergeReturnsTheMergedEntitiesCollection() {
            var entitiesCollection1 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);
            var entitiesCollection2 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);
            var mergedEntitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });

            Assert.AreEqual(
                entitiesCollection1.Merge(entitiesCollection2),
                mergedEntitiesCollection
            );
        }

        [Test]
        public void MergeWhenTheSameEntityExistsInBothEntitiesCollectionsTheValueFromTheGivenEntitiesCollectionIsUsed() {
            var modifiedWidget1 = new Widget(1, "widget1-modified");
            var entitiesCollection1 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);
            var entitiesCollection2 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, modifiedWidget1);
            var mergedEntitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, modifiedWidget1);

            Assert.AreEqual(
                entitiesCollection1.Merge(entitiesCollection2),
                mergedEntitiesCollection
            );
        }

        [Test]
        public void EmptyWhenTheEntitiesCollectionIsEmptyReturnsTrue() {
            var entitiesCollection = new EntitiesCollection();

            Assert.IsTrue(entitiesCollection.Empty);
        }

        [Test]
        public void EmptyWhenTheEntitiesCollectionIsNotEmptyReturnsFalse() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            Assert.IsFalse(entitiesCollection.Empty);
        }

        [Test]
        public void CountReturnsTheNumberOfEntitiesInTheEntitiesCollection() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                })
                .PutEntity(THINGIES_ENTITY_TYPE, THINGY1);

            Assert.AreEqual(
                entitiesCollection.Count,
                3
            );
        }

        [Test]
        public void CastToEntityReferencesCollectionReturnsAnEntityReferencesCollectionWithReferencesToEntitiesInTheEntitiesCollection() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                })
                .PutEntity(THINGIES_ENTITY_TYPE, THINGY1);
            var entityReferencesCollection = (EntityReferencesCollection) entitiesCollection;
            var correctEntityReferencesCollection =
                new EntityReferencesCollection()
                .PutEntityIdsOfType(WIDGETS_ENTITY_TYPE, new HashSet<uint>() {
                    WIDGET1.id,
                    WIDGET2.id
                })
                .PutEntityId(THINGIES_ENTITY_TYPE, THINGY1.id);

            Assert.AreEqual(
                entityReferencesCollection,
                correctEntityReferencesCollection
            );
        }

        [Test]
        public void PlusOperatorMergesTheOperandEntitiesCollections() {
            var entitiesCollection1 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);
            var entitiesCollection2 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);
            var mergedEntitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                });

            Assert.AreEqual(
                entitiesCollection1 + entitiesCollection2,
                mergedEntitiesCollection
            );
        }

        [Test]
        public void EqualsWhenTheEntitiesCollectionsAreEqualReturnsTrue() {
            var entitiesCollection1 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);
            var entitiesCollection2 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);

            Assert.IsTrue(entitiesCollection1.Equals(entitiesCollection2));
        }

        [Test]
        public void EqualsWhenTheEntitiesCollectionsAreNotEqualReturnFalse() {
            var entitiesCollection1 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1)
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET2);
            var entitiesCollection2 =
                new EntitiesCollection()
                .PutEntity(WIDGETS_ENTITY_TYPE, WIDGET1);

            Assert.IsFalse(entitiesCollection1.Equals(entitiesCollection2));
        }

        [Test]
        public void GetEnumeratorReturnsAnEnumeratorOverEachEntityByType() {
            var entitiesCollection =
                new EntitiesCollection()
                .PutEntitiesOfType(WIDGETS_ENTITY_TYPE, new HashSet<IEntity>() {
                    WIDGET1,
                    WIDGET2
                })
                .PutEntity(THINGIES_ENTITY_TYPE, THINGY1);

            var pairs = (
                from pair in entitiesCollection
                select pair
            ).ToList();

            Assert.AreEqual(
                pairs,
                new List<KeyValuePair<string, IEntity>>() {
                    new KeyValuePair<string, IEntity>(WIDGETS_ENTITY_TYPE, WIDGET1),
                    new KeyValuePair<string, IEntity>(WIDGETS_ENTITY_TYPE, WIDGET2),
                    new KeyValuePair<string, IEntity>(THINGIES_ENTITY_TYPE, THINGY1)
                }
            );
        }
    }
}
